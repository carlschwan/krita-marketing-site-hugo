---
title: "Website Information"
date: "2014-07-16"
type: "simple-page"
---

## Marketing Site (krita.org)
This is the central hub where people learn about Krita and download the latest version.
The website is hosted by KDE. The [GIT source code](https://invent.kde.org/scottpetrovic/krita-marketing-site-hugo) is found under KDE's Gitlab instance:



## Documentation Site (docs.krita.org)
This is the educational area hosting Krita’s user manual, reference manual and tutorials, etc.
The website is powered by the Sphinx framework. This website uses the Creative Commons Attribution ShareAlike license, unless otherwise noted.

## Credits
Design and Development – Scott Petrovic
Chinese Translators (Simplified) – Tyson Tan
Chinese Translators (Traditional) – Alvin Wong
Japanese Translators – Tokiedian, Guruguru