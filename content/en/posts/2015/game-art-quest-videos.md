---
title: "Game Art Quest videos!"
date: "2015-12-11"
---

Here's Nathan's overview of the Instant Preview beta:

{{< youtube c9yiBRFQnbo >}}


And the animation toolset:

{{< youtube 9uvju6sUNJA >}}
