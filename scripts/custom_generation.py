import os
import sys
import fileinput

textToSearch = input( "MOLLIEAPIKEY" ) # text to search for
textToReplace = input( "NEW KEY TO GO HERE SOMEWHERE??" ) # text to replace with

# File to perform Search-Replace on
fileToSearch  = 'themes/krita-org-theme/static/php/mollie-one-time-donation.php'

tempFile = open( fileToSearch, 'r+' )

# go through each line and swap out text if found
for line in fileinput.input( fileToSearch ):
    if textToSearch in line :
        tempFile.write( line.replace( textToSearch, textToReplace ) )
    
# close the file for writing
tempFile.close()